public class Card{
	private String ranks;
	private String suits;
		// constructor
	public Card(String ranks, String suits){
		this.ranks = ranks;
		this.suits = suits;
	}
	//Getters
	public String getRank(){
		return this.ranks;
	}
	public String getSuit(){
		return this.suits;
	}
	//toString
	public String toString(){
		return "The card is " + this.ranks + " of " + this.suits;
	}

	public Double calculateScore(){
		Double rank = 0.0;
		Double suit = 0.0;
		
		String [] suitsArray = {"Hearts", "Diamonds", "Spades", "Clubs"};
		Double [] suitPoints = {0.4, 0.3, 0.2, 0.1};
		String [] ranksArray = { "Ace", "Two", "Three","Four", "Five", "Six", "Seven","Eight",
		"Nine", "Ten", "Jack", "Queen", "King"};
		Double [] rankPoints = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0};

		for (int index = 0 ; index < ranksArray.length; index++){
			if (this.ranks.equals(ranksArray[index])){
				rank = rankPoints[index];
			}
		}
		for (int index = 0; index < suitsArray.length ; index ++){
			if (this.suits.equals(suitsArray[index])){
				suit = suitPoints[index];
			}		
		}
		Double totalPoints = rank + suit; 
		return totalPoints;
	}
}   